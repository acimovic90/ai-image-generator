(function () {
  const form = document.querySelector('form');
  const loaderDiv = document.querySelector('.loader');
  const placeholder = document.querySelector('.prompt_container');
  const closeButton = document.querySelector(".close-button");
  const modal = document.querySelector('.modal');
  const backgroundColor = ['#97DE88', '#FF8B83', '#BB8ABF'];
  const rects = document.querySelectorAll('rect');
  const randomPrompts = new Map();
  let modalContent = document.querySelector('.modal_content');
  let loaderActive = true;

  function loader() {
    const color = backgroundColor[Math.floor(Math.random() * backgroundColor.length)];
    for (const rect of rects) {
      rect.setAttribute('fill', color);
    }
    loaderActive = loaderActive == true ? false : loaderDiv.style.visibility = 'hidden';
  }

  fetch('array.txt')
    .then(response => response.text())
    .then(data => {
      const shuffledData = shuffleArray(data.split('\n'));
      for (const [index, item] of shuffledData.slice(0, 25).entries()) {
        randomPrompts.set(index, item.replace(/["',]/g, ''));
        randomPromptButtons(index);
      }
    });

  function generateImage(text) {
    loader();
    const img = document.createElement('img');
    img.src = text;
    modalContent = document.querySelector('.modal_content');
    modalContent.appendChild(img);
  }

  function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  function randomPromptButtons(index) {
    const button = document.createElement('button');
    button.className = `button_prompt_${index}`;
    button.style.backgroundColor = backgroundColor[Math.floor(Math.random() * backgroundColor.length)];
    button.textContent = randomPrompts.get(index);
    const src = document.querySelector('.random_prompts');
    src.appendChild(button);
    button.addEventListener('click', () => {
      placeholder.setAttribute('placeholder', randomPrompts.get(index));
    });
  }


  function handleSubmit(e) {
    e.preventDefault();
    const data = placeholder.getAttribute("placeholder"); //TODO - Right now it only gets text from placeholder attribute and not from self written text. 
    form.reset();
    fetch('http://localhost:5000', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        prompt: data
      })
    })
      .then(response => {
        if (response.ok) {
          return response.json().then(data => generateImage(data.bot.trim()));
        } else {
          return response.text().then(error => {
            alert(error);
            reloadPage();
          });
        }
      });
  }

  function reloadPage() {
    location.reload();
  }

  //Eventlisteners
  form.addEventListener('submit', handleSubmit);
  closeButton.addEventListener('click', reloadPage);
  form.addEventListener('keyup', (e) => {
    if (e.key === 'Enter') {
      modal.classList.toggle('show-modal');
      handleSubmit(e);
      loader();
      closeButton.style.visibility = 'visible';
    }
  });
})();