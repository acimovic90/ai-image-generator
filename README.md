1. This app sends API calls in form of text prompts, to openai, which repsons with a AI generated image.
2. This code was developed with HTML, CSS and Vanilla JS.
3. Other technologies being used are Node.js and openai DALL-E for generating images.
4. See package.json in both client/server directories and install the required modules.
5. Cd into client and write - npm run dev.
6. Cd into server and write - npm run server.
7. You should be able to run it on http://localhost:<port> .
8. Configurate the .env with a valid API key which you can generate easily following this link - https://platform.openai.com/account/api-keys 
9. The app is not mobile responsive, that and many other features may be added in the near future.
10. Enjoy!